module PlanManager

  # Get current user plan
  def current_plan
    account = current_account

    plan_id = account.stripe_plan_id

    if plan_id.nil?
      return nil
    end

    plan = Plan.find_by_stripe_id(plan_id)




  end

  def current_account

    Account.find_by_email(current_user.email)
  end

  def plan_check(action, subject)
    if current_plan.nil?
      # Free plan (No plan)
      check_no_plan_restrictions(action, subject)
    elsif current_plan.stripe_id == "plan-free"
      check_free_plan_restrictions(action, subject)
    elsif current_plan.stripe_id == "plan-good"
      check_good_plan_restrictions(action, subject)
    elsif current_plan.stripe_id == "plan-awesome"
      check_awesome_plan_restrictions(action, subject)
    end
  end

  def check_no_plan_restrictions(action, subject)
    if action == "create"
      if subject == "weather_grid"
        return "You need a plan to create weather grid. Please visit <a href='/subscriptions'>Plans</a> to buy a plan"
      end
    end
  end

  def check_free_plan_restrictions(action, subject)
    if action == "create"
      if subject == "weather_grid"
        count = WeatherGrid.count
        if count >= 2
          return "Your plan only allows 2 weather grids. Please visit <a href='/subscriptions'>Plans</a> to buy a plan"

        end
      end
    end
  end

  def check_good_plan_restrictions(action, subject)
    if action == "create"
      if subject == "weather_grid"
        count = WeatherGrid.count
        ap count
        if count >= 5
          return "Your plan only allows 5 weather grids. Please visit <a href='/subscriptions'>Plans</a> to buy a plan"

        end
      end
    end
  end
  def check_awesome_plan_restrictions(action, subject)
    if action == "create"
      if subject == "weather_grid"
        count = WeatherGrid.count
        if count >= 10
          return "Your plan only allows 10 weather grids. Please visit <a href='/subscriptions'>Plans</a> to buy a plan"

        end
      end
    end
  end


end