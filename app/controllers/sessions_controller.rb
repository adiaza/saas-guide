class SessionsController < Devise::SessionsController

  # Overriding devise session controller to check for subdomain
  # Only allow login within a subdomain
  def new
    if request.subdomain.blank? || request.subdomain == "www"
      flash[:notice] = "Access Restricted"
      redirect_to :root
    else
      super
    end
  end

end