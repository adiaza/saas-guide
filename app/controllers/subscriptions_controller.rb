class SubscriptionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user

  def new
    @plans = Plan.all
  end

  def index
    @account = Account.find_by_email(current_user.email)
  end

  def edit
    @account = Account.find(params[:id])
    @plans  = Plan.all
  end

  def create
    # ap "Inside create in subscription. See params hash"
    # ap params

    # Get the credit card details submitted by the form
    token           = params[:stripeToken]

    plan            = params[:plan][:stripe_id]

    email           = current_user.email

    current_account = Account.find_by_email(current_user.email)
    customer_id     = current_account.customer_id
    current_plan    = current_account.stripe_plan_id

    if customer_id.nil?
      # New customer, create a new customer
      # Create a Customer
      @customer = Stripe::Customer.create(
        :source => token,
        :plan => plan,
        :email => email
      )

      subscriptions   = @customer.subscriptions
      @subscribed_plan = subscriptions.data.find { |o| o.plan.id == plan }

    else
      # Customer exists
      # Get customer from Stripe
      @customer = Stripe::Customer.retrieve(customer_id)
      @subscribed_plan = create_or_update_subscription(@customer, current_plan, plan)


    end

    # Get current period end - This is a unix timestamp
    current_period_end = @subscribed_plan.current_period_end

    # Convert to datetime
    active_until = Time.at(current_period_end).to_datetime

    save_account_details(current_account, plan, @customer.id, active_until)


    redirect_to :root, :notice => "Succesfully subscribed to plan " + plan

  rescue => e
    redirect_to :back, :flash => { :error => e.message }

  end

  def update_card

  end

  def update_card_details
    token           = params[:stripeToken]

    # Get cust id
    current_account = Account.find_by_email(current_user.email)
    customer_id     = current_account.customer_id

    # Get cust from stripe
    customer = Stripe::Customer.retrieve(customer_id)

    # Set new card token
    customer.source = token
    customer.save

    redirect_to '/subscriptions', notice: "Card updated successfully"

  rescue  => e
    redirect_to '/subscriptions/update_card', :flash => { :notice => e.message }

  end

  def cancel_subscription

    current_account = Account.find_by_email(current_user.email)
    customer_id     = current_account.customer_id
    current_plan    = current_account.stripe_plan_id

    if current_plan.blank?
        raise "No plan found to unsubscribe"
    end

    # Fetch customer from stripe
    customer = Stripe::Customer.retrieve(customer_id)

    # Get current subscription
    subscriptions = customer.subscriptions

    # Find the subscription that we want to cancel
    current_subscribed_plan = subscriptions.data.find { |o| o.plan.id == current_plan }

    if current_subscribed_plan.blank?
      raise "Subscription not found"
    end

    # Delete subscription
    current_subscribed_plan.delete

    # Udpdate account model
    save_account_details(current_account, "", customer_id, Time.at(0).to_datetime)

    @message = "Subscription cancelled successfully"

    rescue => e
      redirect_to "/subscriptions", :flash => { :error => e.message }


  end

  def save_account_details(account, plan, customer_id, active_until)

    # Customer created with a valid subscription
    # So, update account model wit details

    account.stripe_plan_id = plan
    account.customer_id    = customer_id
    account.active_until   = active_until
    account.save!
  end

  def create_or_update_subscription(customer, current_plan, new_plan)
    subscriptions = customer.subscriptions
    # Get current subscription
    current_subscription = subscriptions.data.find { |o| o.plan.id == current_plan }

    if current_subscription.blank?
      # No current subscription
      # Maybe the customer unsubscribed prvlsy or maybe the card was declined
      # So, create a new subscription to existing customer
      subscription = customer.subscriptions.create({:plan => new_plan})
    else
      # Existing subscription found
      # Must be an upgrade or downgrade
      # So update existing subscription with new plan
      current_subscription.plan = new_plan
      subscription = current_subscription.save
    end

    return subscription
  end

  # Auth user or raise exception
  def authorize_user
    authorize! :manage, :subscriptions
  end

end
